//
//  DataManager.m
//  Quizz
//
//  Created by Andre Ferreira dos Santos on 17/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

+(id)getDataManager
{
    static DataManager *datamanger;
    @synchronized(self)
    {
        if(datamanger == nil)
        {
            datamanger = [DataManager new];
            [datamanger restoreConfigDefault];
        }
    }
    return datamanger;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _dict = [NSMutableDictionary new];
    }
    return self;
}

-(void)restoreConfigDefault
{
    NSString *_true = @"true";
    NSString *_false = @"false";
    
    [_dict setValue:_true  forKey:@"pt_br"];
    [_dict setValue:_false forKey:@"en_us"];
    [_dict setValue:_true  forKey:@"easy"];
    [_dict setValue:_true  forKey:@"normal"];
    [_dict setValue:_false forKey:@"hard"];
    
}

-(NSString *)LoadStringFromFile:(NSString *)file useBundle:(BOOL)bundle{
    
    
    NSString *documentPath;
    if(bundle){
        documentPath = [[NSBundle mainBundle].bundlePath stringByAppendingPathComponent:file];
    }else{
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentPath = [[paths objectAtIndex:0]stringByAppendingPathComponent:file];
    }
    
    NSError *error;
    
    NSString *contentOfFile = [[NSString alloc] initWithContentsOfFile:documentPath encoding:NSUTF8StringEncoding error:&error];
    
    if(contentOfFile == nil){
        NSLog(@"Erro reading file:%@",[error description]);
    }
    
    return contentOfFile;
    
}

// essa funcao vai retornar um dicionario com uma chave (numero da pergunta) armazenando um array com: 4 opcoes e a resposta certa
-(NSMutableDictionary *)fetchQuestions
{
    NSMutableDictionary *res = [NSMutableDictionary new];
    NSString *str_questions = @"";
    NSString *ret;
    NSString *val;
    
    //checa as configuraroes adicionando as perguntas ao indice de questoes
    if([[_dict valueForKey:@"pt_br"] isEqualToString:@"true"])
    {
        if([[_dict valueForKey:@"easy"] isEqualToString:@"true"])
        {
            val = str_questions;
            ret = [self LoadStringFromFile:@"easy_pt.csv" useBundle:YES];
            str_questions = [NSString stringWithFormat:@"%@%@",val,ret];
        }
        if([[_dict valueForKey:@"normal"] isEqualToString:@"true"])
        {
            val = str_questions;
            ret = [self LoadStringFromFile:@"normal_pt.csv" useBundle:YES];
            str_questions = [NSString stringWithFormat:@"%@%@",val,ret];
        }
        if([[_dict valueForKey:@"hard"] isEqualToString:@"true"])
        {
            val = str_questions;
            ret = [self LoadStringFromFile:@"hard_pt.csv" useBundle:YES];
            str_questions = [NSString stringWithFormat:@"%@%@",val,ret];
        }
    }
    if([[_dict valueForKey:@"en_us"] isEqualToString:@"true"])
    {
        if([[_dict valueForKey:@"easy"] isEqualToString:@"true"])
        {
            val = str_questions;
            ret = [self LoadStringFromFile:@"easy_en.csv" useBundle:YES];
            str_questions = [NSString stringWithFormat:@"%@%@",val,ret];
        }
        if([[_dict valueForKey:@"normal"] isEqualToString:@"true"])
        {
            val = str_questions;
            ret = [self LoadStringFromFile:@"normal_en.csv" useBundle:YES];
            str_questions = [NSString stringWithFormat:@"%@%@",val,ret];
        }
        if([[_dict valueForKey:@"hard"] isEqualToString:@"true"])
        {
            val = str_questions;
            ret = [self LoadStringFromFile:@"hard_en.csv" useBundle:YES];
            str_questions = [NSString stringWithFormat:@"%@%@",val,ret];
        }
    }
    
    //Me retorna um array com os blocos de perguntas, compostos por: questao, resposta certa, e 4 opcoes
    NSArray *questions = [str_questions componentsSeparatedByString:@"\n"];
    
    NSUInteger n_questions = [questions count];
    
    for(NSUInteger i=0;i<(n_questions-1);i++)
    {
        NSArray *block = [[questions objectAtIndex:i] componentsSeparatedByString:@";"];
        
        NSArray *pergunta = @[
            [block objectAtIndex:1], //questao
            [block objectAtIndex:2], //resposta certa
            [block objectAtIndex:3], //opcao 1
            [block objectAtIndex:4], //opcao 2
            [block objectAtIndex:5], //opcao 3
            [block objectAtIndex:6], //opcao 4
            [block objectAtIndex:7],  //nome arquivo musica
        ];
        
        NSString *key = [block objectAtIndex:0];
        
        [res setObject:pergunta forKey:key];
    }
    
    
    return res;
}


@end
