//
//  MenuVC.h
//  Quizz
//
//  Created by Andre Ferreira dos Santos on 20/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuVC : UIViewController <UITextFieldDelegate>

@property NSString *name_text;

@end
