//
//  MenuVC.m
//  Quizz
//
//  Created by Andre Ferreira dos Santos on 20/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import "MenuVC.h"
#import "DataManager.h"

@interface MenuVC ()

@end

@implementation MenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"shattered_"]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UiTextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    _name_text = textField.text;
    return [textField resignFirstResponder];
}


@end
