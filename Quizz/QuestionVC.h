//
//  QuestionVC.h
//  Quizz
//
//  Created by Andre Ferreira dos Santos on 19/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface QuestionVC : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, AVAudioPlayerDelegate> {
    //propriedade da musica
    AVAudioPlayer *mus;
    AVAudioPlayer *acertou;
    AVAudioPlayer *errou;
}

//outlets
@property (weak, nonatomic) IBOutlet UILabel *pergunta;
@property (weak, nonatomic) IBOutlet UIButton *play_Button;
@property (weak, nonatomic) IBOutlet UIButton *stop_button;
@property (weak, nonatomic) IBOutlet UILabel *musicas;
@property (weak, nonatomic) IBOutlet UILabel *acertos;

//propriedades de pergunta
@property NSString *pergunta_text;
@property NSString *resposta_certa;
@property NSArray *opcoes;
@property NSInteger opcaoAtual;
@property NSString *pathSound;
@property NSNumber *pontos;
@property NSNumber *atual;
@property NSNumber *max;

//acoes de botoes
- (IBAction)restart:(id)sender;

- (IBAction)tocar:(id)sender;
- (IBAction)stop:(id)sender;

@end
