//
//  QuestionVC.m
//  Quizz
//
//  Created by Andre Ferreira dos Santos on 19/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import "QuestionVC.h"
#import "DataManager.h"
#import "parabensVC.h"

@interface QuestionVC ()

@end

@implementation QuestionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    _max = [[[DataManager getDataManager]dict]objectForKey:@"maxPontos"];
    _atual = [[[DataManager getDataManager]dict]objectForKey:@"musAtual"];
    
    _pontos = [[[DataManager getDataManager]dict]objectForKey:@"pontos"];
    
    _musicas.text = [ NSString stringWithFormat:@"Músicas: %ld / %ld",(long)[_atual intValue], (long)[_max intValue]];
    _acertos.text = [NSString stringWithFormat:@"Acertos: %ld",(long)[_pontos intValue]];
    
    //imagem de background
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"shattered_"]];
    
    //sortea uma das questoes do data manager
    NSMutableDictionary *perguntas = [[[DataManager getDataManager]dict]valueForKey:@"perguntas"];
    
    NSArray *array = [perguntas allKeys];
    
    NSString *rnd_key;
    NSMutableArray *perguntaSorteada = [NSMutableArray new];
    
    int rnd;
    
    NSLog(@"%ld",(long)[array count]);

    rnd = arc4random()%[array count];
    
    rnd_key = [array objectAtIndex:rnd];
    
    //pergunta sorteada para esse "round"
    perguntaSorteada = [perguntas objectForKey:rnd_key];
    
    
    //joga os valores da pergunta sorteada para a view
    _pergunta_text = perguntaSorteada[0];
    _resposta_certa = perguntaSorteada[1];
    _opcoes = @[perguntaSorteada[2], perguntaSorteada[3], perguntaSorteada[4], perguntaSorteada[5]];
    _pathSound = perguntaSorteada[6];
    
    
    //remove essa pergunta da lista de perguntas disponiveis
    [perguntas removeObjectForKey:rnd_key];
    //joga esse novo dicionario de perguntas para o dataManager
    [[[DataManager getDataManager]dict]setObject:perguntas forKeyedSubscript:@"perguntas"];
    
    //inicia a musica que sera tocada
    NSString *path = [[NSBundle mainBundle]pathForResource:_pathSound ofType:@"wav"];
    
    mus = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    mus.delegate = self;
    mus.numberOfLoops = 0;
    
    //som do acertou
    path = [[NSBundle mainBundle]pathForResource:@"acertou" ofType:@"wav"];
    
    acertou = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    acertou.delegate = self;
    acertou.numberOfLoops = 0;
    
    //som do errou
    path = [[NSBundle mainBundle]pathForResource:@"errou" ofType:@"wav"];
    
    errou = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    errou.delegate = self;
    errou.numberOfLoops = 0;
    
    //Coloca nos outlets as variaveis
    _pergunta.text = _pergunta_text;
    
    _play_Button.alpha = 1;
    _play_Button.enabled = YES;
    
    _stop_button.alpha = 0;
    _stop_button.enabled = NO;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)restart:(id)sender {
    
    [mus stop];
    
    if([_atual intValue] > [_max intValue] -1)
    {
        NSString * storyboardName = @"Main";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
        parabensVC * vc = [storyboard instantiateViewControllerWithIdentifier:@"parabens"];
        [self presentViewController:vc animated:YES completion:nil];
    }
    else
    {
    
        int actual = [_atual intValue];
        actual += 1;
        NSNumber *nb = [NSNumber numberWithInt:actual];
        [[[DataManager getDataManager]dict]setValue:nb forKey:@"musAtual"];
    
    
        if([_opcoes[_opcaoAtual] isEqualToString:_resposta_certa])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ACERTOU" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            
            //[acertou play];
        
            int pontos = [_pontos intValue];
            pontos += 1;
            NSNumber *nb = [NSNumber numberWithInt:pontos];
            [[[DataManager getDataManager]dict]setValue:nb forKey:@"pontos"];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ERROU" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            
            //[errou play];
        }
    
        NSString * storyboardName = @"Main";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
        QuestionVC * vc = [storyboard instantiateViewControllerWithIdentifier:@"questionVC"];
        //[self viewDidLoad];
        [self presentViewController:vc animated:YES completion:nil];
    }
}

- (IBAction)tocar:(id)sender {
    
    [mus play];
    
    _play_Button.alpha = 0;
    _play_Button.enabled = NO;
    
    _stop_button.alpha = 1;
    _stop_button.enabled = YES;
    
}

- (IBAction)stop:(id)sender {
    [mus stop];
    _play_Button.alpha = 1;
    _play_Button.enabled = YES;
    
    _stop_button.alpha = 0;
    _stop_button.enabled = NO;
}

#pragma mark PrepareforSegue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [mus stop];
}


#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_opcoes count];
}

#pragma mark UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _opcoes[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _opcaoAtual = row;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
