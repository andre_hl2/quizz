//
//  ViewController.h
//  Quizz
//
//  Created by Andre Ferreira dos Santos on 17/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISwitch *pt_br;
@property (weak, nonatomic) IBOutlet UISwitch *en_us;

@property (weak, nonatomic) IBOutlet UISwitch *easy;
@property (weak, nonatomic) IBOutlet UISwitch *normal;
@property (weak, nonatomic) IBOutlet UISwitch *hard;

@end

