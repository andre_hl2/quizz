//
//  ViewController.m
//  Quizz
//
//  Created by Andre Ferreira dos Santos on 17/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import "ViewController.h"
#import "DataManager.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"shattered_"]];

    
    [DataManager getDataManager];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    //SET THE CONFIGS
    if(_pt_br.on == YES)
        [[[DataManager getDataManager]dict]setValue:@"true" forKey:@"pt_br"];
    else
        [[[DataManager getDataManager]dict]setValue:@"false" forKey:@"pt_br"];
    
    if(_en_us.on == YES)
        [[[DataManager getDataManager]dict]setValue:@"true" forKey:@"en_us"];
    else
        [[[DataManager getDataManager]dict]setValue:@"false" forKey:@"en_us"];
    
    
    if(_easy.on == YES)
        [[[DataManager getDataManager]dict]setValue:@"true" forKey:@"easy"];
    else
        [[[DataManager getDataManager]dict]setValue:@"false" forKey:@"easy"];
    
    if(_normal.on == YES)
        [[[DataManager getDataManager]dict]setValue:@"true" forKey:@"normal"];
    else
        [[[DataManager getDataManager]dict]setValue:@"false" forKey:@"normal"];
    
    if(_hard.on == YES)
        [[[DataManager getDataManager]dict]setValue:@"true" forKey:@"hard"];
    else
        [[[DataManager getDataManager]dict]setValue:@"false" forKey:@"hard"];
    

    //FETCH THE QUESTIONS WITH THE NEW CONFIGS
    NSMutableDictionary *perguntas = [[DataManager getDataManager]fetchQuestions];
    
    [[[DataManager getDataManager]dict]setObject:perguntas forKey:@"perguntas"];
    
    //contabiliza o maximo de pontos que ele pode ter
    NSNumber *count = [NSNumber numberWithInteger:[perguntas count]];
    [[[DataManager getDataManager]dict]setValue:count forKey:@"maxPontos"];
    
    //zera os pontos
    NSNumber *pontos = [NSNumber numberWithInteger:0];
    [[[DataManager getDataManager]dict]setValue:pontos forKey:@"pontos"];
}

@end
