//
//  bateriaPerguntasVC.h
//  Quizz
//
//  Created by Andre Ferreira dos Santos on 21/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface bateriaPerguntasVC : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *musicasDisponiveis;

@property NSMutableArray *possibleSets;
@property NSInteger maxOption;
@property NSInteger opcaoAtual;

@end
