//
//  bateriaPerguntasVC.m
//  Quizz
//
//  Created by Andre Ferreira dos Santos on 21/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import "bateriaPerguntasVC.h"
#import "DataManager.h"

@interface bateriaPerguntasVC ()

@end

@implementation bateriaPerguntasVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"shattered_"]];
    
    NSMutableDictionary *perguntas = [[[DataManager getDataManager]dict]objectForKey:@"perguntas"];
    
    NSUInteger number = [perguntas count];
    
    _maxOption  = number;
    
    _musicasDisponiveis.text = [NSString stringWithFormat:@"Voce tem %ld musicas disponíveis",(long)_maxOption];
    
    _possibleSets = [[NSMutableArray alloc]initWithCapacity:7];
    
    [_possibleSets insertObject:@"Todas" atIndex:0];
    
    if(number >= 5)
        [_possibleSets insertObject:@"5 músicas" atIndex:1];
    if(number >= 10)
        [_possibleSets insertObject:@"10 músicas" atIndex:2];
    if(number >= 15)
        [_possibleSets insertObject:@"15 músicas" atIndex:3];
    if(number >= 20)
        [_possibleSets insertObject:@"20 músicas" atIndex:4];
    if(number >= 25)
        [_possibleSets insertObject:@"25 músicas" atIndex:5];
    if(number >= 30)
        [_possibleSets insertObject:@"30 músicas" atIndex:6];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_possibleSets count];
}

#pragma mark UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _possibleSets[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _opcaoAtual = row;
}


#pragma mark navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if(_opcaoAtual == 0)
    {
        NSNumber *count = [NSNumber numberWithInteger:_maxOption];
        [[[DataManager getDataManager]dict]setValue:count forKey:@"maxPontos"];
    }
    else if (_opcaoAtual == 1)
    {
        NSNumber *count = [NSNumber numberWithInteger:5];
        [[[DataManager getDataManager]dict]setValue:count forKey:@"maxPontos"];
    }
    else if(_opcaoAtual == 2)
    {
        NSNumber *count = [NSNumber numberWithInteger:10];
        [[[DataManager getDataManager]dict]setValue:count forKey:@"maxPontos"];
    }
    else if(_opcaoAtual == 3)
    {
        NSNumber *count = [NSNumber numberWithInteger:15];
        [[[DataManager getDataManager]dict]setValue:count forKey:@"maxPontos"];
    }
    else if(_opcaoAtual == 4)
    {
        NSNumber *count = [NSNumber numberWithInteger:20];
        [[[DataManager getDataManager]dict]setValue:count forKey:@"maxPontos"];
    }
    else if(_opcaoAtual == 5)
    {
        NSNumber *count = [NSNumber numberWithInteger:25];
        [[[DataManager getDataManager]dict]setValue:count forKey:@"maxPontos"];
    }
    else
    {
        NSNumber *count = [NSNumber numberWithInteger:30];
        [[[DataManager getDataManager]dict]setValue:count forKey:@"maxPontos"];
    }
    NSNumber *nb = [NSNumber numberWithInt:1];
    [[[DataManager getDataManager]dict]setValue:nb forKey:@"musAtual"];
}

@end
