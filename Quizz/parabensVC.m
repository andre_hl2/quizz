//
//  parabensVC.m
//  Quizz
//
//  Created by Andre Ferreira dos Santos on 21/03/15.
//  Copyright (c) 2015 Andre Ferreira dos Santos. All rights reserved.
//

#import "parabensVC.h"
#import "DataManager.h"

@interface parabensVC ()

@end

@implementation parabensVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"shattered_"]];

    
    NSNumber *counter = [[[DataManager getDataManager]dict]objectForKey:@"pontos"];
    
    _acertou.text = [NSString stringWithFormat:@"Voce acertou %ld questoes",(long)[counter intValue]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
